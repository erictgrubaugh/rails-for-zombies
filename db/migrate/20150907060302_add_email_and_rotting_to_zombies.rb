class AddEmailAndRottingToZombies < ActiveRecord::Migration
  def up
    add_column :zombies, :email, :string, unique: true
    add_column :zombies, :rotting, :boolean, default: false
    change_column :zombies, :name, :string, null: false, unique: true
  end
  def down
    remove_column :zombies, :email, :string, unique: true
    remove_column :zombies, :rotting, :boolean, default: false
    change_column :zombies, :name, :string, null: true, unique: false
  end
end
